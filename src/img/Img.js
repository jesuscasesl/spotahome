import React, { Component } from 'react';

import './Img.css';

class Img extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <img className="photo" src={ this.props.url } alt={ this.props.alt }/>
    )
  }
}

export default Img;
