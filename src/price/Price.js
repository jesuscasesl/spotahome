import React, { Component } from 'react';

import './Price.css';

class Price extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <span className="price">{ this.props.txtPrice }</span>
    )
  }
}

export default Price;
