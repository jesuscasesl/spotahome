import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from './../button/Button';
import Img from './../img/Img';
import Title from './../title/Title';
import Price from './../price/Price';


import './Room.css';

class Room extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="room">

        <Img
          url={this.props.phto}
          alt={ "home" }/>

        <div className="room-info">
          <Title
            txtTitle={this.props.title}/>

          <div className="info-price">
            <Price
              txtPrice={this.props.price}/>
          </div>

          <div className="info-buttons">
            <Button
              cl="green"
              txtBtn="More details"/>
            <Button
              cl="orange"
              txtBtn="Book now!"/>
          </div>
        </div>
      </div>
    )
  }
}

Room.propTypes = {
  id: PropTypes.number,
  phto: PropTypes.string,
  title: PropTypes.string,
  txtPrice: PropTypes.number,
  cl: PropTypes.number,
  txtBtn: PropTypes.number
};

export default Room;
