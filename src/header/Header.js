import React from 'react';
import './Header.css';

const Header = () => {
  return (
    <header className="header">
      <h1 className="header-title">Spot<span className="title-special">a</span>room</h1>
      <span className="header-subTitle">the best wayto find your home</span>
    </header>
  )
}

export default Header;
