export function orderBy(array, order='asc', by='pricePerMonth') {
  if(array.some(item => !item.hasOwnProperty(by))){
    by='pricePerMonth';
  }

  const ordered = array.sort((a,b) => {
  	if(typeof a[by] ==='string')
    	return a[by].localeCompare(b[by])
    return a[by] - b[by]
  })

	if(order === 'desc') {
		return ordered.reverse()
  }

  return ordered
}
