import React, { Component } from 'react';

import './Title.css';

class Title extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <p className="title">{ this.props.txtTitle }</p>
    )
  }
}

export default Title;
