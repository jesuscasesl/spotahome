import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Dropdown from './../dropdown/Dropdown';

import './Filter.css';

class Filter extends Component {
  constructor(props) {
    super(props)

    this.byPrice = [
      {
        key: "asc",
        value: "asc"
      },
      {
        key: "desc",
        value: "desc"
      }
    ]

    this.byProperty = [
      {
        key: 'all',
        value: "all"
      },
      {
        key: "room_shared",
        value: "Room"
      },
      {
        key: "apartaments",
        value: "Apartaments"
      }
    ]
  }

  handleOnPropertyChange(value){
    this.props.onSelectProperty(value)
  }

  handleOnPriceChange(value){
    this.props.onSelectPrice(value)
  }

  render() {
    return (
        <div className="filters">
          <h2 className="filters-title">Filters:</h2>
          <div className="filters-dropdown">
            <Dropdown
              txt="Property type:"
              options={this.byProperty}
              onChange={(value) => this.handleOnPropertyChange(value)}/>
            <Dropdown
              txt='Sort by price:'
              options={this.byPrice}
              onChange={(value) => this.handleOnPriceChange(value)}/>
          </div>
        </div>
    )
  }
}

Filter.propTypes = {
  onSelectProperty: PropTypes.func,
  onSelectPrice: PropTypes.func
};

export default Filter;
