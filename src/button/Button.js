import React, { Component } from 'react';

import './Button.css';

class Button extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <button className={ this.props.cl } type="button">{ this.props.txtBtn }</button>
    )
  }
}

export default Button;
