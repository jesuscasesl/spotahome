import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Container from './../container/Container';
import Header from './../header/Header';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="app">
        <Header/>
        <Router>
            <Switch>
              <Route exact path="/" component={Container}/>
              <Route exact path="/:id" component={Container}/>
            </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
