import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Dropdown.css';

class Dropdown extends Component {
  constructor(props) {
    super(props)
  }

  change(event){
    this.props.onChange(event.target.value)
  }

  render() {
    return (
      <div className="dropdown">
        <span className="dropdown-title">{ this.props.txt }</span>
        <select id="dropdown-options" className="dropdown-options" onChange={(event) => this.change(event)}>
          {
            this.props.options.map( opt => (
                <option key={opt.key} value={opt.key}>{opt.value}</option>
            ))
          }
        </select>
      </div>
    )
  }
}

Dropdown.propTypes = {
  txt: PropTypes.string,
  options: PropTypes.array,
  onChange: PropTypes.func
};

export default Dropdown;
