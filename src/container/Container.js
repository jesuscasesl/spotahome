import React, { Component } from 'react';

import Filter from './../filter/Filter';
import ListRooms from './../listRooms/ListRooms';

import { orderBy } from './../helper';

import './Container.css';

class Container extends Component {
  constructor(props) {
    super(props)
    this.state = {
      rooms: [],
      initialRooms: [],
      order: 'asc',
      loading: true,
      error: false
    }
  }

  componentWillMount(){
    return fetch('https://www.spotahome.com/api/public/listings/similars/122836')
      .then((response) => {
        return response.json()
      })
      .then((spotahomeResponse) => {
        return this.setState({
          initialRooms: spotahomeResponse.data.homecards,
          rooms: spotahomeResponse.data.homecards,
          loading: false,
          error: false
        })
      })
      .then(() => {
        this.orderBy(this.state.order)
      })
      .catch((a) => {
        this.setState({
          error: true,
          loading: false
        })
      })
  }

  filterBy (criteria) {
    if (criteria === 'all') {
      return this.setState(
        { rooms: this.state.initialRooms },
        () => this.orderBy(this.state.order)
      )
    }

    this.orderBy(this.state.order)
    const roomsFiltered = this.state.initialRooms.filter(items => items.type === criteria);
    this.setState({ rooms: roomsFiltered })
  }

  orderBy (criteria = undefined) {
    const roomsOrdered = orderBy(this.state.rooms, criteria);
    this.setState({
      rooms: roomsOrdered,
      order: criteria
    })
  }

  render() {
    return (
        <main className="main">
          <aside className="aside">
            <Filter
              rooms={this.state.rooms}
              onSelectProperty={(criteria) => this.filterBy(criteria)}
              onSelectPrice={(criteria) => this.orderBy(criteria)}
            />
          </aside>
          <section className="section">
            <ListRooms
              rooms={this.state.rooms}/>
          </section>
        </main>
    )
  }
}

export default Container;
