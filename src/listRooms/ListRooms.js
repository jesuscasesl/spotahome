import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Room from './../room/Room';

import './ListRooms.css';

class ListRooms extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <article className="listRooms">
        {
          this.props.rooms.map( room =>  {
            return <Room
              key = { room.id }
              id = { room.id }
              adId = { room.adId }
              price = { room.pricePerMonth }
              phto = { room.mainPhotoUrl }
              title = { room.title}
            />
          })
        }
      </article>
    )
  }
}

ListRooms.propTypes = {
  rooms: PropTypes.array
};

export default ListRooms;
